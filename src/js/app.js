import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready
  const hotPrices=Array.from(document.querySelectorAll(".hot"))
  hotPrices.forEach(element => {
    element.textContent=element.textContent+'🔥';
    // console.log(element.textContent)
  });
});
